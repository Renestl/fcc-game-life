import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { ButtonToolbar, Button, MenuItem, DropdownButton } from 'react-bootstrap';

class Box extends Component {
	render() {
		return (
			<div
				className={this.props.boxClass}
				id={this.props.id}
				onClick={() => this.props.selectBox(this.props.row, this.props.col)}
			/>
		);
	}
}

class Grid extends Component {
	render() {
		// # of cols * 14 to build grid size
		const width = (this.props.cols * 14);
		var rowsArr = [];

		var boxClass = '';

		// multidimensional array
		// can be done with .map
		for (var i = 0; i < this.props.rows; i++) {
			for (var j = 0; j < this.props.cols; j++) {
				let boxId = i + "_" + j;

				// checks if box at this spot in grid is on or off
				// and assigns css classes
				boxClass = this.props.gridFull[i][j] ? "box on" : "box off";
				rowsArr.push(
					<Box
						boxClass={boxClass}
						key={boxId}
						boxId={boxId}
						row={i}
						col={j}
						selectBox={this.props.selectBox}
					/>
				);
			}
		}

		return (
			<div className="grid" style={{width: width}}>
				{rowsArr}
			</div>
		)
	}
}

class Buttons extends Component {
	handleSelect = (event) => {
		this.props.gridSize(event);
	}


	render() {
		return(
			<div className="center">
				<ButtonToolbar>
					<Button onClick={this.props.playButton}>Play</Button>
					<Button onClick={this.props.pauseButton}>Pause</Button>
					<Button onClick={this.props.clear}>Clear</Button>
					<Button onClick={this.props.slow}>Slow</Button>
					<Button onClick={this.props.fast}>Fast</Button>
					<Button onClick={this.props.seed}>Seed</Button>				
					<DropdownButton
						title="Grid Size"
						id='size-menu'
						onSelect={this.handleSelect}
					>
						<MenuItem eventKey='1'>20x10</MenuItem>
						<MenuItem eventKey='2'>50x30</MenuItem>
						<MenuItem eventKey='3'>70x50</MenuItem>
						<MenuItem eventKey='4'>100x80</MenuItem>						
					</DropdownButton>
				</ButtonToolbar>
			</div>
		)
	}
}

class Main extends Component {
	constructor() {
		super();

		// will be called by the state
		this.speed = 100;
		this.rows = 30;
		this.cols = 50;

		this.state = {
			generation: 0,
			gridFull: Array(this.rows).fill().map(() => Array(this.cols).fill(false))
		}
	}

	selectBox = (row, col) => {
		// copy of array through arrayClone function
		let gridCopy = arrayClone(this.state.gridFull);
		gridCopy[row][col] = !gridCopy[row][col];

		this.setState({
			gridFull: gridCopy
		})
	}

	seed = (row, col) => {
		let gridCopy = arrayClone(this.state.gridFull);
		
		for (let i = 0; i < this.rows; i++) {
			for (let j = 0; j < this.cols; j++) {
				if (Math.floor(Math.random() * 4) === 1) {
					gridCopy[i][j] = true;
				}
			}
		}

		this.setState({
			gridFull: gridCopy
		});
	}

	playButton = () => {
		clearInterval(this.intervalId);
		this.intervalId = setInterval(this.play, this.speed);
	}

	pauseButton = () => {
		clearInterval(this.intervalId);
	}

	slow = () => {
		this.speed = 1000;
		this.playButton();
	}

	fast = () => {
		this.speed = 100;
		this.playButton();
	}

	clear = () => {
		var grid = Array(this.rows).fill().map(() => Array(this.cols).fill(false));
		this.setState({
			gridFull: grid,
			generation: 0
		});
		this.pauseButton();
	}

	gridSize = (size) => {
		switch (size) {
			case "1":
				this.cols = 20;
				this.rows = 10;
			break;
			case "2":
				this.cols = 50;
				this.rows = 30;
			break;
			case "3":
				this.cols = 70;
				this.rows = 50;
			break;
			default:
				this.cols = 100;
				this.rows = 80;
		}
		this.clear();
		this.seed();
		this.playButton();
	}

	play = () => {
		let grid1 = this.state.gridFull;
		let grid2 = arrayClone(this.state.gridFull);

		for (let i = 0; i < this.rows; i++) {
			for (let j = 0; j < this.cols; j++) {
				// keeps track of neighbor cells
				let count = 0;
				// rules to determine how many neigbors there are out of 8 potential
		    if (i > 0) if (grid1[i - 1][j]) count++;
		    if (i > 0 && j > 0) if (grid1[i - 1][j - 1]) count++;
		    if (i > 0 && j < this.cols - 1) if (grid1[i - 1][j + 1]) count++;
		    if (j < this.cols - 1) if (grid1[i][j + 1]) count++;
		    if (j > 0) if (grid1[i][j - 1]) count++;
		    if (i < this.rows - 1) if (grid1[i + 1][j]) count++;
		    if (i < this.rows - 1 && j > 0) if (grid1[i + 1][j - 1]) count++;
				if (i < this.rows - 1 && this.cols - 1) if (grid1[i + 1][j + 1]) count++;
				// determine if cell lives or dies
		    if (grid1[i][j] && (count < 2 || count > 3)) grid2[i][j] = false;
		    if (!grid1[i][j] && count === 3) grid2[i][j] = true;
			}
		}

		this.setState({
			gridFull: grid2,
			generation: this.state.generation + 1
		})
	}

	componentDidMount() {
		this.seed();
		this.playButton();
	}

	render() {
		return (
			<div>
				<h1>The Game of Life</h1>

				<Grid 
					gridFull={this.state.gridFull}
					rows={this.rows}
					cols={this.cols}
					selectBox={this.selectBox}
				/>

				<h2>Generations: {this.state.generation}</h2>
				<Buttons 
					playButton={this.playButton}
					pauseButton={this.pauseButton}
					slow={this.slow}
					fast={this.fast}
					clear={this.clear}
					seed={this.seed}
					gridSize={this.gridSize}
				/>
			</div>
		);
	}
}

// deep clone nested array
function arrayClone(arr) {
	return JSON.parse(JSON.stringify(arr));
	// return grid.map(arr => arr.slice());
}

ReactDOM.render(<Main />, document.getElementById('root'));

